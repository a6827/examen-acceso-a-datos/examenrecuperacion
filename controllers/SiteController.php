<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1(){
        
        $dataProvider = new ActionDataProvider([
             
         'query'=>Ciclista::find()->Select('numero')->from('etapas')->where('salida=llegada and kms=(Select MAX(kms) from etapas')
            
        ]);
 
        return $this->render ('resultado',[
           'resultados'==$dataProvider,
            'campos'==['numero'],
            'sql'=='Select numero from etapas where salida=llegada and kms=(Select MAX(kms) from etapas)'   
        ]);
    }
    
    
    public function actionConsulta2(){
        
        $dataProvider = new ActionDataProvider([
            
        'sql'=>Ciclista::find()->Select('nombre')->from('ciclistas')->where('edad =(Select MAX(edad) from ciclistas)')
        ]);
        
        return $this->render ('resultado',[
           'resultados'==$dataProvider,
            'campos'==['nombre'],
            'sql'=='Select nombre from Banesto where edad =(Select MAX(edad) from ciclistas)'   
        ]);
        
        
    }
    
    
    public function actionConsulta3(){
        
        $dataProvider = new ActionDataProvider([
            
        'sql'=>Ciclista::find()->Select('puerto')->from('puertos')->where('categoria=1 and altura=(Select MAX(altura) from puertos))')
        ]);
        
        return $this->render ('resultado',[
           'resultados'==$dataProvider,
            'campos'==['nombre'],
            'sql'=='Select nombre from Banesto where edad =(Select MAX(edad) from ciclistas)'   
        ]);       
}
}

//Select numero from etapas where salida=llegada and kms=(Select MAX(kms) from etapas)

//Select nombre from Banesto where edad =(Select MAX(edad) from ciclistas)

//Select puerto from puertos where categoria=1 and altura=(Select MAX(altura) from puertos)